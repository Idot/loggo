package mapper

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

//用来表示类型的数据类型
type Type uint

const (
	Int Type = iota + 1
	Float
	String
)

var tArray = []string{"", "int", "float", "string"}

//interface stringer
func (t *Type) String() string {
	return tArray[t]
}

var ErrType = errors.New("mapper:没有此类型！")

func NewType(t string) (Type, error) {
	t = strings.ToLower(t)
	for index, value := range tArray {
		if value == t {
			return Type(index), nil
		}
	}
	return 0, ErrType
}

// 保存转换成数据结构的日志片段
// Name 是给片段的名称，不可重复
// Type 是该片段的类型
// value 是该片段对应的原始值
type Block struct {
	Name  string
	Type  Type
	value interface{}
}

func (t *Block) Int() (int64, error) {
	if t.Type != Int {
		return 0, fmt.Errorf("Type of this %s,not %s", t.Type, Int)
	}
	return t.value.(int64), nil
}
func (t *Block) Float() (float64, error) {
	if t.Type != Float {
		return 0, fmt.Errorf("Type of this %s,not %s", t.Type, Float)
	}
	return t.value.(float64), nil
}
func (t *Block) String() (string, error) {
	if t.Type != String {
		return "", fmt.Errorf("Type of this %s,not %s", t.Type, String)
	}
	return t.value.(string), nil
}

type Line map[string]Block

func (l *Line) Get(key string) Block {
	b, ok := l[key]
	if ok {
		return b
	}
	return nil
}

func (l *Line) Set(key string, b Block) bool {
	if _, ok := l[key]; ok {
		return false
	}
	l[key] = b
	return true
}

func (l *Line) Replace(key string, b Block) {
	l[key] = b
}

type File struct {
	name  string
	lines []Line
}

func (f *File) Name() string {
	return f.name
}

func (f *File) Line(num uint) Line {
	if num >= len(f.lines) {
		return nil
	}
	return f.lines[num]
}

// 用来描述用户自定义日志片段的数据结构
// Name 用来描述片段名称的，不可重复
// Type 用来描述片段的类型
// Pos 用来描述该片段在日志中的位置
type m struct {
	Name string
	Type string
	Pos  int
}
type configuration struct {
	err error
	Sep string
	Map []m
}
