package calculator

import (
	"fmt"
)

//整型求和
func SumI(is ...Inter) int64 {
	var total int64
	for _, i := range is {
		v, err := i.Int()
		if err != nil {
			fmt.Println(err)
			continue
		}
		total += v
	}
	return total
}

//浮点数求和
func SumF(fs ...Floater) float64 {
	var total float64
	for _, f := range fs {
		v, err := f.Float()
		if err != nil {
			fmt.Println(err)
			continue
		}
		total += v
	}
	return total
}

//求和，可以是整型也可以是浮点型
func Sum(ns ...Number) float64 {
	var total float64
	for index, n := range ns {
		f, err := n.Float()
		if err != nil {
			i, err := n.Int()
			if err != nil {
				fmt.Println("This not number! index:", index)
			} else {
				f = float64(i)
			}
		}
		total += f
	}
	return total
}
