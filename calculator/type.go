package calculator

type Typer interface {
	Inter
	Floater
	Stringer
}

type Number interface {
	Inter
	Floater
}

type Inter interface {
	Int() (int64, error)
}

type Floater interface {
	Float() (float64, error)
}

type Stringer interface {
	String() (string, error)
}
