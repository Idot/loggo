package main

import (
	"bufio"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
)

const (
	TimeFormat = "2006-01-02 15:04:05"
	LogLength  = 12
)

var (
	ErrLen = errors.New("the number of fields is not fixed!")
)

func main() {
	logText := `2015-03-19 00:00:10:INFO http-apr-8080-exec-11967 com.tpad.billing.sdk.debug.channel - 00000109001	CMCC	10.133.157.245	null	null	460027448843345	865951020211189`
	l := new(log3005)
	l.Scan(logText)
	if l.Err() != nil {
		log.Println(l.Err())
	}
	log.Printf("%v", l)
}

type LogFile struct {
	Files []os.FileInfo
	index int

	err error
}

func (l *LogFile) ReadDir(dirname string) {
	files := new(os.FileInfo)
	files, l.err = ioutil.ReadDir(dirname)
	l.Files = append(l.Files, files...)
}

func (l *LogFile) ReadFile(filename string) {
	file := new(os.File)
	file, l.err = os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	var info os.FileInfo
	info, l.err = file.Stat()
	l.Files = append(l.Files, info)
}

func (l *LogFile) Next() bool {
	flag := len(l.Files) < l.index
	l.index++
	return flag
}

func (l *LogFile) Read(scanner LogScanner) {
	info := l.Files[l.index]
	info.
}

func (l *LogFile) Err() error {

}

func (l *LogFile) Reset() {
	var files []os.FileInfo
	l.Files = files
	l.index = 0
}

type LogScanner interface {
	Scan(text string)
	Err() error
}

type log3005 struct {
	Time          time.Time
	ChannelId     string
	Carrier       string
	Ip            string
	Tel           string
	ProvinceIndex string
	Imsi          string
	Imei          string

	err error
}

func (l *log3005) Scan(text string) {
	strs := strings.Fields(text)
	if len(strs) != LogLength {
		l.err = ErrLen
		return
	}

	timeStr := strs[0] + " " + strings.Replace(strs[1], ":INFO", "", -1)
	l.Time, l.err = time.Parse(TimeFormat, timeStr)
	l.ChannelId = strs[5]
	l.Carrier = strs[6]
	l.Ip = strs[7]
	l.Tel = strs[8]
	l.ProvinceIndex = strs[9]
	l.Imsi = strs[10]
	l.Imei = strs[11]
}

func (l *log3005) Err() error {
	return l.err
}
