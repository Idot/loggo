package main

import (
	"fmt"

	cal "loggo/calculator"
	"loggo/mapper"
	"loggo/setting"
)

const (
	LOG = `119.49.199.247 - - [09/Jul/2014:10:17:15 +0800] "GET /UMS/files/d9455339d16842279444f932e3029d0d/icon_neihanmimasuo.jpg HTTP/1.1" 200 5284 "http://www.uichange.com/UMS/home.action?v=3.3.2&r=540*960&imei=863034024048717&imsi=460023432009416&st=lock&fm=com.yingyonghui.market&dm=0&l=zh&net=WIFI&op=YD&g=female" "Mozilla/5.0 (Linux; U; Android 4.2.2; zh-cn; Lenovo A708t Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"`
)

func main() {
	fmt.Println("Test base funcations.")
	fmt.Println("Loading settings...")
	s, err := setting.Load("setting.json")
	if err != nil {
		fmt.Println(err)
		return
	}

	res := mapper.Map(LOG, s.Sep, s.Fields)
	for _, f := range s.Fields {
		fmt.Println(res[f.Name])
	}

	n := s.Fields[0].Name
	fmt.Println(cal.SumI(res[n]))
}
