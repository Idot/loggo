package setting

import (
	"encoding/json"
	"os"
)

func Load(name string, v interface{}) (err error) {
	f, err := os.Open(name)
	if err != nil {
		return
	}
	defer f.Close()

	return json.NewDecoder(f).Decode(v)
}
